﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TimerTrigger : MonoBehaviour
{
    public TextFileManager fileManager;

    private float timer = -1;
    private bool counting = false;
    
    
    //creates the file for that game when it starts. it will set the starting variables and hide the input field
    //setting 'counting' to true allows for the timer to start when the oject is loaded.
    void Start()
    {
        fileManager.CreateFile(fileManager.logName);
        fileManager.ReadFileContents(fileManager.logName);
        timer = 0;
        counting = true;
        UIManager.instance.input.gameObject.SetActive(false);// starts with no input active
        
    }

    //checks when the player touches the exit point, if they do it will activate the input field allowing the player to add a score
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && counting == true)
        {
            counting = false;
            UIManager.instance.input.gameObject.SetActive(true);// starts with no input active
        }
        counting = false;
    }

    //This should update the timer that is shown on the players screen. also contains the time for the highscores page in the menu.
    void Update()
    {
        if (counting == true)
        {
            timer += Time.deltaTime;
            UIManager.instance.UpdateTimer(timer);
        }//if

    }

    /// <summary>
    /// This collects the name and time and saves them into its file. It also allows for high scores to be overwritten. 
    /// It is set up so it only holds 3 scores.
    /// After a score from the input field has been entered it will save and kick to menu.
    /// </summary>
    public void SaveTimeToFile()
    {
        //fileManager.AddKeyValuePair(fileManager.logName, UIManager.instance.input.text, timer.ToString());
        
        for (int i = 0; i < 3; i++)
        {
            if (fileManager.LocateValueByKey("HighScore" + i.ToString()) == "")
            {
                if (fileManager.LocateValueByKey(UIManager.instance.input.text) == "")
                {
                    fileManager.AddKeyValuePair(fileManager.logName, "HighScore" + i.ToString(), UIManager.instance.input.text);
                    fileManager.AddKeyValuePair(fileManager.logName, UIManager.instance.input.text, timer.ToString());
                    break;
                }
                else
                {
                    float.TryParse(fileManager.LocateValueByKey(UIManager.instance.input.text), out float r);
                    if(r > timer)
                    {
                        fileManager.AddKeyValuePair(fileManager.logName, UIManager.instance.input.text, timer.ToString());
                        continue;
                    }
                }
            }
            else
            {
                
                string k = fileManager.LocateValueByKey("HighScore" + i.ToString());
                if (k == UIManager.instance.input.text)
                {
                    fileManager.AddKeyValuePair(fileManager.logName, k , timer.ToString());
                    break;
                }
            }
        }
        UIManager.instance.input.gameObject.SetActive(false);
        SceneManager.LoadScene(0); 
    }

}

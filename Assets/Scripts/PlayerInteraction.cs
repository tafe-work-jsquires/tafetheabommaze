﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInteraction : MonoBehaviour
{
    public float distance = 3.5f;
    public Image crosshair;
    public Image chInteract;
   

    void Start()
    {

        chInteract.fillAmount = 0;

    }//Start End


    void Update()
    {
        CrossHairUpdate();
        PlayerInteract();

    }// Update end


    /// <summary>
    /// Sends out a raycast, if it hits a door it will change the crosshair 
    /// this is done by changing the fill amount
    /// it could be done by changing the colour too if needed
    /// </summary>
    public void CrossHairUpdate()
    {
        //this is for changing the crosshair if you can interact with the object
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out RaycastHit crossH, distance * 2) == true)
        {
            ObjectInteractions obj = crossH.transform.GetComponent<ObjectInteractions>();

            if (obj == null)
            {
                obj = crossH.transform.GetComponentInParent<ObjectInteractions>();
            }
            if (obj is DoorInteraction door)
            {
                chInteract.fillAmount = 1;
                crosshair.fillAmount = 0;

            }
        }
        else
        {
            chInteract.fillAmount = 0;
            crosshair.fillAmount = 1;
        }
    }

    /// <summary>
    /// This will send out a ray cast for every frame that the Use key is pressed
    /// it will then check if the ray hits an object that is a door
    /// It will then send the Activate() abstract to that objects script
    /// </summary>
    public void PlayerInteract()
    {

        //this is for sending the activation after pressing the Use key.
        if (Input.GetButtonDown("Use") == true)
        {
            //                                                   local direction                       moving for the distance
            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out RaycastHit hit, distance) == true)
            {
                //not needed in the official build as debug can be laggy.
                //Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.forward * distance, Color.green, 0.5f); // 2nd = Direction, 3rd = set a colour, 4 = set a duration
                
                ObjectInteractions obj = hit.transform.GetComponent<ObjectInteractions>();//lets the object use the Activate() abstract from the ObjectInteractionClass

             //if the object is a child this will allow it to no longer be null
                if (obj == null)
                {
                    obj = hit.transform.GetComponentInParent<ObjectInteractions>();
                }
                
             //this will activate the object after the raycast hits
                if (obj != null)
                {
                    //checks if this is a door
                    if (obj is DoorInteraction door)
                    {
                        // we will be checking our keys  
                        if (door.locked == true)
                        {
                            if (GetComponent<KeyRing>().CheckedKeys(door.colour) == true)
                            {
                                door.UnlockDoor(true);
                            }

                        }
                        else
                        {
                            door.Activate();
                        }
                    }
                    else
                    {
                        obj.Activate();
                    }

                }//end if3

            }//end if2

        }//end if1
    }

}//heat death of the universe

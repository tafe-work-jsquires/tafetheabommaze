﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LogReader : MonoBehaviour
{
    public TextFileManager fileManager;

    //creates the folder and file that is needed
    private void Start()
    {
        fileManager.CreateFile(fileManager.logName);//this will create a folder in assets called resources and will fill it with a log that has be named.
        fileManager.ReadFileContents(fileManager.logName);
       
    }

}

[System.Serializable]
public class TextFileManager
{
    public string logName;
    public string[] logContents;

   //creates a file within the resources folder of the build
    public void CreateFile(string fileName)
    {
        // we know that the assets folder is where everything is. so we need to find out where the asset folder is.
        string dirPath = Application.dataPath + "/Resources/" + fileName + ".txt";

        if (File.Exists(dirPath) == false)
        {
            Directory.CreateDirectory(Application.dataPath + "/Resources/"); // will be ignored if alread exists
            File.WriteAllText(dirPath, fileName + '\n'); // '\n' is a new line, this will write all to the dirpath with the name of the file.
        }

    }


    /// <summary>
    /// this will read the file that is specified
    /// it will then next that to the Menu so that it can be displayed
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public string[] ReadFileContents(string fileName)
    {
        string dirPath = Application.dataPath + "/Resources/" + fileName + ".txt";
        string[] textContents =new string[0];

        if (File.Exists(dirPath) == true) // this will check if it exists and will then fill the textContents
        {
            textContents = File.ReadAllLines(dirPath);
        }

        logContents = textContents;
        
        return textContents;
    }

    /// <summary>
    /// gets three values
    /// 1 the file name so the information can be stored
    /// 2 a key so that the name can be associated with the time
    /// 3 a value that is the time
    /// this will combine the value to the name in order for the data to be read out 
    /// </summary>
    /// <param name="fileName"></param>
    /// <param name="key"></param>
    /// <param name="value"></param>
    public void AddKeyValuePair (string fileName, string key, string value)
    {
        ReadFileContents(fileName);//keeps track of the most recent file.
        string dirPath = Application.dataPath + "/Resources/" + fileName + ".txt";
        string content = key + "," + value;
        string timeStamp = System.DateTime.Now.ToString(); // keeps track of the date and time


        if (File.Exists(dirPath) == true) // this will check if it exists and will then fill the textContents
        {
            bool contentsFound = false;

            for (int i= 0; i < logContents.Length; i++)// i stands for index, it will increase from 0(the starting position) to the length of the log.
            {
                if (logContents[i].Contains(key + ",") == true)
                {
                    logContents[i] = timeStamp + " - " + content;// sets the current position of the array to this
                    contentsFound = true;
                    break; //stops the for code
                }//if
            }//for

            if (contentsFound == true)
            {
                File.WriteAllLines(dirPath, logContents);
            }//if
            else
            {
                File.AppendAllText(dirPath, timeStamp + " - " + content + '\n');//adds a line to the end of the file
            }//else
        }//if
    } 


    /// <summary>
    /// checks the log and splits the data so it can be read as seperate values
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public string LocateValueByKey (string key)
    {
        string value = "";

        ReadFileContents(logName);

        foreach(string s in logContents)
        {
            if (s.Contains(key + ",") == true)
            {
                string[] splitString = s.Split(','); // it will look for the comma and split the line
                value = splitString[splitString.Length - 1];
                break;
            }//if
        }//for

        return value;

    }//method


    /// <summary>
    /// Create a temp array that is empty
    /// create a local string to store a value of the CSV
    /// Read the file
    /// For each line check if the key and ',' are there
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public string[] GetPairByte (string key)
    {
        string[] r = new string[0];
        string value = "";
        ReadFileContents(logName);

        foreach (string s in logContents)
        {
            if (s.Contains(key + ",") == true)
            {
                string[] splitstring = s.Split(',');
                value = splitstring[splitstring.Length - 1];
                r = new string[] { key, value };

                break;
            }
        }

        return r;
    }


    /// <summary>
    /// Removes all text from the file
    /// this will allow for new scores to be added
    /// </summary>
    /// <param name="fileName"></param>
    public void ClearFile (string fileName)
    {
        string dirPath = Application.dataPath + "/Resources/" + fileName + ".txt";
        File.WriteAllText(dirPath, fileName + '\n');
    }

}
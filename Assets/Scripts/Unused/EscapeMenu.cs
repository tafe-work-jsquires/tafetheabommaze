﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EscapeMenu : MonoBehaviour
{

    public GameObject gameQuitMenu; // allows us to turn off the menu so it does not block the screen

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// When the player enters the input for the Menu button a set of ui will open and the timer must pause
    /// 
    /// </summary>
    public void OpenMenu()
    {
        if (Input.GetButtonDown("Menu") == true)
        {



        }


    }

    public void QuitGame()
    {
        SceneManager.LoadScene(0);
    }

}

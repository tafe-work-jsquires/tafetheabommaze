﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Image healthBar;
    public Text healthText;
    public Text scoreText;
    public Text timerText;
    public InputField input;
    // a singleton is a class that can only have one instance of itself, this is handy since we can get can use static variables pretty easily.
    public static UIManager instance; // handy but should be used rarely. There should only ever be one of this.

    private void Awake()
    {

        if(instance == null)
        {
            instance = this;
        }

    }
    //updates the score although this is not implemented
    public void UpdateScore (int score)
    {

        scoreText.text = "Score: " + score;

    }

    //updates the health for the UI when the player takes damage or heals
    public void UpdateHealth(Health health)
    {
        healthBar.fillAmount = health.CurrentHealth / health.maxHealth;
        if (health.CurrentHealth > 0)
        {
            healthText.text = "Health: " + health.CurrentHealth + " / " + health.maxHealth;
        }
        else
        {
            healthText.text = "Dead";
        }
    }

    //changes the time to what is sent per frame from TimerTrigger
    public void UpdateTimer(float time)
    {
        timerText.text = time.ToString();
    }

    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hazard : MonoBehaviour
{
    public float damage = 25;//a base damage will need to be changed in a case to case
    public bool tick;//allows for the damage to be dealt per frame. With this off damage only occurs once per trigger. 
    public bool heal;//this will allow damage to be reversed 


    private void OnTriggerEnter(Collider other)
    {
     
        if(other.tag == "Player")// if the player enters the area that is the hazard they will take 1 tick of damage to their health.
        {
            if (heal)
            {
                other.GetComponent<Health>().TakeDamage(-1*damage);
            }
            else 
            {
                other.GetComponent<Health>().TakeDamage(damage);
            }
        }
    }


    private void OnTriggerStay(Collider other)
    {
        
        if (tick == true) //this will trigger every tick while the player is withing the trigger zone
        {
            if (other.tag == "Player") 
            {
                if (heal)
                {
                    other.GetComponent<Health>().TakeDamage(-1 * damage);
                }
                else
                {
                    other.GetComponent<Health>().TakeDamage(damage);
                }
            }
        }
    }

}// end class

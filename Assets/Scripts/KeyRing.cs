﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyRing : MonoBehaviour
{
    [SerializeField]//allows the inspector to view this private, must be above the variable
    private List<KeyColour> collectedKeys = new List<KeyColour>();

    // Start is called before the first frame update
    void Start()
    {
        //collectedKeys.Add(KeyColour.red); not goood
        //Debug.Log(collectedKeys.count); gives nothing

    }

    /// <summary>
    /// when a key is triggered it will add it to the key ring.
    /// this will allow the player to open doors of the same colour.
    /// </summary>
    /// <param name="colour"></param>
    /// <returns></returns>
    public bool PickupKey(KeyColour colour)
    {
        if (collectedKeys.Contains(colour) == false) //checks if key is in list
        {
            collectedKeys.Add(colour);
            return true;
        }
        return false;
        //when you return you will end the code. Anything under the return will be terminated/ignored
    }

    /// <summary>
    /// stops keys of the same colour for being added more than once
    /// </summary>
    /// <param name="colour"></param>
    /// <returns></returns>
    public bool CheckedKeys( KeyColour colour)
    {
        foreach(KeyColour c in collectedKeys)
        {
            if (c == colour)
            {
            return true;
            }
        }
        return false;
    }
}

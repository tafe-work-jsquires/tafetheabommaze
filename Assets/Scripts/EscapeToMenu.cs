﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EscapeToMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        OpenMenu();
    }

    /// <summary>
    ///  Pressing the Menu key will send the player to the Main menu
    ///  the default key is 'escape'
    /// </summary>

    public void OpenMenu()
    {
        if (Input.GetButtonDown("Menu") == true)
        {

            SceneManager.LoadScene(0); // switch to the menu scene at scene 0

        }


    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackToMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ButtonBackToMenu();
    }

    /// <summary>
    /// When the player hits the escape key (aka the Menu key) it will return to the main menu
    /// </summary>
    public void ButtonBackToMenu()
    {
        if (Input.GetButtonDown("Menu") == true)
        { 
        SceneManager.LoadScene(0); // scene 0 is the main menu
        }
    
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public TextFileManager fileManager;
    public GameObject scoreWindow;
    public Text scoreText;

    //turns off the score window at the load point.
    private void Awake()
    {
        if (scoreWindow.activeSelf == true)
        {
            ToggleScoreWindow();
        }

    }

    /// <summary>
    /// used with the button in the menu for scores
    /// will collect the scores that have been stored by the TimerTrigger
    /// will show both the time and name of the player.
    /// </summary>
    private void LoadScores()
    {
        fileManager.ReadFileContents(fileManager.logName);
        scoreText.text = null;
        for (int i = 0; i < 3; i++)
        {
            string[] s = fileManager.GetPairByte("HighScore" + i.ToString());
            if (s.Length > 0)
            {
                scoreText.text += '\n' + fileManager.GetPairByte(s[1])[0].ToUpper() + " " + fileManager.GetPairByte(s[1])[1];
            }
        }
    }


    /// <summary>
    /// Flips the score windo on and off
    /// </summary>
    public void ToggleScoreWindow() 
    {
        LoadScores();
        scoreWindow.SetActive(!scoreWindow.activeSelf);
    }

    /// <summary>
    /// changes the scene so that it is now the play scene
    /// </summary>
    public void StartGame() 
    {
        SceneManager.LoadScene(1);
    }

    /// <summary>
    /// closes the application.
    /// Only works when built.
    /// </summary>
    public void Quit() 
    {
        Application.Quit();
    }

    /// <summary>
    /// clears the scores.
    /// allowing for new ones to be added.
    /// </summary>
    public void ClearFile() 
    {
        fileManager.ClearFile(fileManager.logName);
    }
}
